package com.devcreativa.customers.services;

import com.devcreativa.customers.models.dtos.CustomerDTO;

public interface CustomerService extends BaseService<CustomerDTO>{

}
