# Customer manager - RestFull Service

### Proyect content

**RestFull Service: Crud project of a client entity.**  

* Model
* Repository
* Service
* Controller
* Swagger test
* Unit tests
* Spring security

### Tecnologis content

* Spring boot - v2.6.2
* Java 11
* Swagger v3.0.0
* Jacoco v0.8.5
* Checkstyle v3.1.2
* Junit 5

